com.conformiq.creator.structure.v15
creator.gui.screen qmlc68c7843c2bf4ae89def36c01bea774a "Login "
{
	creator.gui.form qmlf34fcb3575c247b992ac387aca2ec8fa "Customer Login"
	{
		creator.gui.textbox qml505cc3dbde314909a1aa51856e812f3e "UserName"
			type = String
			status = dontcare;
		creator.gui.textbox qml892b15a9ede041119a55a7441ead3a59 "unnamed"
			type = String
			status = dontcare
			deleted;
		creator.gui.textbox qmlc8348b95c28649979a3512483057758c "Password"
			type = String
			status = dontcare;
		creator.gui.checkbox qmld34c338e38a2438f84612377ca2b2567 "unnamed"
			status = dontcare
			checked = dontcare
			deleted;
	}
	creator.gui.button qml5011da8ac4844af289853ab636514313 "Login"
		status = dontcare;
}
creator.gui.screen qmlcca35242373d40739d9c7c3f9975b47c "Account Services"
{
	creator.gui.hyperlink qmlb8048eac378d4bdeaf13cbf3f3b40b24 "Open New Account"
		status = dontcare;
	creator.gui.hyperlink qmlc5cc9e92c9cd47f0bb4c400d5ecefe9d "Bill Pay"
		status = dontcare;
	creator.gui.hyperlink qmlf2ea669fc712470d871af470c77bde05 "Logout"
		status = dontcare;
}
creator.gui.screen qmle481fcc7811e4a3aac0f4b85598c2eec "Open New Account"
{
	creator.gui.form qml876ce8b6577e44abaf58f53292887690 "Open New Account"
	{
		creator.gui.dropdown qmlff916ddcc7884464831a911d6f3c88b2 "Type of Account"
			type = qml65aafe07ed6c41c6864a56efc531e06d
			status = dontcare;
		creator.gui.dropdown qml5fe81530da054408a1b38f2cf6149d05 "Existing Account"
			type = qml3845ced827764ce6a4c25efa5f00c763
			status = dontcare;
	}
	creator.gui.form qml9102dad3f9c94bb5887ed82fa6df8c1c "Existing Account"
		deleted
	{
		creator.gui.dropdown qml930a4273c2aa4b8ca7dc3714cc1b090a "Existing Account"
			status = dontcare
			deleted;
	}
	creator.gui.button qmlb7bd7200cb8844c190c53e2677bf12f5 "Open New Account"
		status = dontcare;
}
creator.enum qml65aafe07ed6c41c6864a56efc531e06d "Type of Account"
{
	creator.enumerationvalue qmlb223a02da2ef47fc84a8b498d24f3a43 "CHECKING";
	creator.enumerationvalue qmlc84d2f546b2842cba863e885382f001f "SAVINGS";
}
creator.enum qml3845ced827764ce6a4c25efa5f00c763 "Existing Account"
{
	creator.enumerationvalue qml714991e3afc94b8b9858789438a6cdf1 "17340";
	creator.enumerationvalue qml60ca2c2cea0b4e7f9aa810d597ff4dde "17784";
}
creator.gui.screen qml6f1fd49f0842494eb6e5207f2eb511e8 "Transfer Funds"
{
	creator.gui.form qml02ac93a0ccd84b12aa251200fe53ed4a "Transfer Funds"
	{
		creator.gui.button qml537caa7ccd9d467e874b187f527f66e1 "Amount"
			status = dontcare
			deleted;
		creator.gui.textbox qmlfd17962d38b5419c9c3053c5a5592b05 "Amount"
			type = String
			status = dontcare;
		creator.gui.dropdown qmld33c0dc272054764beeb18e4b8b340ff "From Account"
			type = qml3845ced827764ce6a4c25efa5f00c763
			status = dontcare;
		creator.gui.textbox qml2574cdf4be9c4043ab2c62a2d8844aad "To Account"
			type = String
			status = dontcare
			deleted;
		creator.gui.dropdown qmlbc815998646b4084ad2e25060d51d19b "To Account"
			type = qml3845ced827764ce6a4c25efa5f00c763
			status = dontcare;
		creator.gui.textbox qml986517ca7c4e47d5a4998a5c2a5138a0 "InWords"
			type = String
			status = dontcare;
	}
	creator.gui.button qml7e979b71b37143d8b6056d3765cda018 "Transfer"
		status = dontcare;
}
creator.gui.screen qml01223365572349c1a7ad3f1c86668efb "MakeMyTrip"
{
	creator.gui.uitable qmla7430b7047ab4100bb11cedeea4c091a "Flight Details"
	datatable = qml8fec594e051648c4a66a4f40578aaa80
	{
		creator.gui.hyperlink qml8a61f0156d1d4cb3a460e0ce32f82943 "Flight Details"
			status = dontcare;
		creator.gui.button qmladed6515ebfc4136b6a22b97f3407091 "View Prices"
			status = dontcare;
	}
}
creator.datatable qml8fec594e051648c4a66a4f40578aaa80 "Flight Info"
{
	creator.primitivecell qml36f156bbf99c4054b278336172532456 "Flight Name"
		type = String;
	creator.primitivecell qml9092f60d3d4d4aaf9fc2234d010401c9 "FromPlace"
		type = String;
	creator.primitivecell qml77bd0384c13b4bc98cfd19ae380bf76a "ToPlace"
		type = String;
	creator.primitivecell qmla390ac0e44a841bd9511405f2135ea10 "Flightchange"
		type = boolean;
	creator.primitivecell qmle6f578f5c6874c4aa462c9e84ee30e12 "Price"
		type = number;
}
creator.gui.screen qml89d324ca8a624a45947cf89e119f2c6a "OrderDetails"
{
	creator.gui.form qmlbf253163824a4eb5a4df7faaeb7fdc95 "Order"
	{
		creator.gui.textbox qml18488f37ae0a4ef887fc43e698647d86 "OrderID"
			type = String
			status = dontcare;
		creator.gui.textbox qmldb890aa253644b4c84e3808dabb93afa "OrderName"
			type = String
			status = dontcare;
	}
	creator.gui.button qml23cb13aa662f4937a281c0c17512c062 "Order"
		status = dontcare;
}
creator.customaction qml63e3f3a1389746079bb1b92ab0cc148a "RightClickonARow"
	interfaces = [ qmlcc7e6400f16e4f9aa11d56aeb23d12ec ]
	shortform = "RC"
	direction = in
	tokens = [ literal "Perform the Right Click" ]
{
}
creator.externalinterface qmlcc7e6400f16e4f9aa11d56aeb23d12ec "User_Input"
	direction = in;
creator.customaction qml3bbebcdbf1a440628d66031ec0c63aeb "Verify Data "
	interfaces = [ qmlb2185cdd8813490892c3849817fdede7 ]
	shortform = "VD"
	direction = out
	tokens = [ literal "Verifying the data that is present" ]
{
	creator.primitivefield qmlcec83d5f42fd47c0b75021c4540f382b "Status"
		type = String;
}
creator.externalinterface qmlb2185cdd8813490892c3849817fdede7 "Verify_Data"
	direction = out;
creator.gui.screen qml357d617a071b4c3d8c40206329b4cff5 "Account"
{
	creator.gui.labelwidget qml7fa7e378fe734e13a8a2c371b429d84e "Account Status"
		status = dontcare;
	creator.gui.hyperlink qml4f2ba6283a814023a2a199243e65a2ac "Account number"
		status = dontcare
		deleted;
	creator.gui.labelwidget qml17971d97ce11458c850460352b929657 "Account Number"
		status = dontcare;
}