com.conformiq.creator.structure.v15
creator.gui.popup qml97488a818e914c3796bf6c0d33288bc3 "Delete_File"
{
	creator.gui.form qml12048f40fe764c3d99de41481d73e6a7 "Delete"
		deleted
	{
		creator.gui.labelwidget qmlc83ea72aa5a24db4a452a62d4c528f95
		"Are you sure want to delete"
			status = dontcare
			deleted;
		creator.gui.button qml5be0df2fa4e345e68545b6399f6744dc "Delete"
			status = dontcare
			deleted;
		creator.gui.button qmla607f10fcb414a618f0ee382fad3a7dd "Cancel "
			status = dontcare
			deleted;
		creator.gui.labelwidget qmlfbc3c6f7ad2d43e98e261d9be3f9e8c2
		"File is moved to recycle bin"
			status = dontcare
			deleted;
		creator.gui.button qml7991d9cddc5849258e01d8e1b5b1c67a "Cross"
			status = dontcare
			deleted;
		creator.gui.radiobutton qmlc42ef22508b6499bb2432e2f1449dbae "Yes_No"
			type = qmld5f57e73cbb24a37ae68d5b80ecbb4d0
			status = dontcare
			deleted;
		creator.gui.labelwidget qml7f81e3957bf24ca39584c31adab1ff37
		"File moved to Recycle bin"
			status = dontcare
			deleted;
		creator.gui.radiobutton qml89f05a70153641dbbcdbd9c12ecfc557 "Yes_No"
			type = qmld5f57e73cbb24a37ae68d5b80ecbb4d0
			status = dontcare
			deleted;
		creator.gui.labelwidget qml469995c2b17c4843a14235b21ebaeceb "Status"
			status = dontcare
			deleted;
		creator.gui.labelwidget qml817028e4e2dc4fc3990e61da479cea23 "Delete_Cancel"
			status = dontcare
			deleted;
		creator.gui.labelwidget qml93327e33dd5c4252850979a11cf70e93
		"Are you sure want to delete"
			status = dontcare
			deleted;
		creator.gui.labelwidget qml96ada4395bae457d93a229cdcfb18734 "Delete_Cancel"
			status = dontcare
			deleted;
		creator.gui.button qml09beae365f354a94a879980a6c08e8b2 "Cross"
			status = dontcare
			deleted;
	}
	creator.gui.button qml64e479345e8d4d909e6996be9d4ba69f ""
		status = dontcare
		deleted;
	creator.gui.button qml180a7b77479743a985379cfaf57312c0 "Cross"
		status = dontcare
		deleted;
	creator.gui.labelwidget qml0487aa942a37491e89c4a4a6dcaebe69
	"File moving is cancelled"
		status = dontcare
		deleted;
	creator.gui.labelwidget qml7d2127d59ba94ec987acae5be6994d8e
	"Are you sure want to delete"
		status = dontcare
		deleted;
	creator.gui.labelwidget qml86842a1b3f974abaae35532d7fe23d5d
	"File is moved to recycle bin"
		status = dontcare
		deleted;
	creator.gui.labelwidget qml1883ba339dda474db7cf8311dcaa9622
	"File moving is cancelled"
		status = dontcare
		deleted;
	creator.gui.button qml3db03e98935b44669e339b58db1682c2 "unnamed"
		status = dontcare
		deleted;
	creator.gui.button qmld9dafd1b37b3410e8d01a77d8dbe780e "unnamed"
		status = dontcare
		deleted;
	creator.gui.labelwidget qmle5479a9965ce4327863e21e350d61e1f
	"Are you sure want to delete"
		status = dontcare;
	creator.gui.labelwidget qml3acf98ae53ae44f4bdf1e6dcdfe49776 "Delete_Cancel"
		status = dontcare
		deleted;
	creator.gui.button qml4fde5e8fc4c44bd5a97c0cb1d251052a "unnamed"
		status = dontcare
		deleted;
	creator.gui.button qml151417aedcec43a6a35ec3fd1a82edf8 "Delete"
		status = dontcare;
	creator.gui.button qml4fae888fd7d34ca9a639c595bf323c98 "Cancel"
		status = dontcare;
	creator.gui.button qml64a87133e4d24de49fba9c305022b418 "unnamed (1)"
		status = dontcare
		deleted;
	creator.gui.button qml875aa815aad14446af624460bd31509e "Cross"
		status = dontcare;
}
creator.enum qmld5f57e73cbb24a37ae68d5b80ecbb4d0 "Yes_No_Values"
	deleted
{
	creator.enumerationvalue qmle131dcbef177411793aac85a84098fe4 "Yes"
		deleted;
	creator.enumerationvalue qmlf7864abb7119412b949618215938ac66 "No"
		deleted;
}